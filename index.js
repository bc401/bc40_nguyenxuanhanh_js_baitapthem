// Bài 1

// Ngày hôm trước
function ngayhomtruoc() {
var nhapngay = document.getElementById("txt-nhap-ngay").value * 1;
var nhapthang = document.getElementById("txt-nhap-thang").value * 1;
var nhapnam = document.getElementById("txt-nhap-nam").value * 1;
var ketqua = document.getElementById("result");
    switch (nhapthang) {
        case 5: case 7: case 10: case 12:
           if(nhapngay == 1) {
            nhapngay = 30;
            nhapthang--;
           } else if (nhapngay > 1) {
            nhapngay--;
           }
        break;
        case 1: case 2: case 4: case 6: case 8: case 9: case 11:
            if(nhapngay == 1 && nhapthang == 1) {
                nhapngay = 31;
                nhapthang = 12;
                nhapnam--;
               }  else if (nhapngay > 1 ) {
                nhapngay--;
               }
        break;
        case 3:
            if ((nhapnam % 4 == 0 && nhapnam % 100 != 0) || nhapnam % 400 == 0) { 
                if (nhapngay == 1 && nhapthang == 3) {
                    nhapngay = 29;
                    nhapthang--;
                    nhapnam--;
                } else if (nhapngay > 1) {
                    nhapngay--;
                   }
            } else if (nhapngay == 1 && nhapthang == 3) {
                nhapngay = 28;
                nhapthang--;
                nhapnam--;
            } else if (nhapngay > 1) {
                nhapngay --;
            }
        break;
    }
ketqua.innerHTML = `${nhapngay}.${nhapthang}.${nhapnam}`;
} 
// Ngày hôm sau
function ngayhomsau() {
var nhapngay = document.getElementById("txt-nhap-ngay").value * 1;
var nhapthang = document.getElementById("txt-nhap-thang").value * 1;
var nhapnam = document.getElementById("txt-nhap-nam").value * 1;
var ketqua = document.getElementById("result");
    switch (nhapthang) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            if(nhapngay == 31 && nhapthang == 12) {
                nhapngay = 01;
                nhapthang = 01;
                nhapnam ++;
               }  else if (nhapngay == 31) {
                nhapngay = 01;
                nhapthang++;
               } else if (nhapngay < 31) {
                nhapngay++;
               }
        break;
        case 4: case 6: case 9: case 11: 
            if(nhapngay == 30) {
                nhapngay = 01;
                nhapthang ++;
               } else if (nhapngay < 30){
                nhapngay++;
               }
        break;
        case 2:
            if ((nhapnam % 4 == 0 && nhapnam % 100 != 0) || nhapnam % 400 == 0) { 
                if (nhapngay == 29 && nhapthang == 2) {
                    nhapngay = 01;
                    nhapthang++;
                } else if (nhapngay < 29) {
                    nhapngay++;
                } else {
                }
            } else if (nhapngay < 29) {
                nhapngay ++;
            } else {

            }
        break;
        default:
            ketqua.innerHTML = `Không có ngày ${nhapngay}.`
            ketqua.innerHTML = `Không có tháng ${nhapthang}.`

    }
ketqua.innerHTML = `${nhapngay}.${nhapthang}.${nhapnam}`;
}

// Bài 2
function tinhngay() {
    var nhapThang = document.getElementById("txt-nhap-thang01").value * 1;
    var nhapNam = document.getElementById("txt-nhap-nam01").value * 1;
    var ketqua = document.getElementById("result1");

    switch (nhapThang) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: 
            if(nhapThang <=12) {
                ketqua.innerHTML = `Tháng ${nhapThang}.${nhapNam} có: 31 ngày.`
            } else {
                ketqua.innerText = null;
            }
            break;
        case 4: case 6: case 9: case 11:
            if(nhapThang <=12) {
                ketqua.innerHTML = `Tháng ${nhapThang}.${nhapNam} có: 30 ngày.`
            } else {
                ketqua.innerText = null;
            }
            break;
        case 2:
            if(nhapNam % 4 == 0 && nhapNam % 100 != 0 || nhapNam % 400 == 0) {
                if(nhapThang <= 12) {
                    ketqua.innerHTML = `Tháng ${nhapThang}.${nhapNam} có: 29 ngày.`
                } else {
                    ketqua.innerText = null;
                }
            } else {
                ketqua.innerHTML = `Tháng ${nhapThang}.${nhapNam} có: 28 ngày.`
            }
            break;
            default:
                ketqua.innerHTML = `Không có tháng ${nhapThang}.`
    }
}

// Bài 3
function docchu() {
    var nhapSo = document.getElementById("txt-nhap-so").value * 1;
    var ketqua = document.getElementById("result2" );

    var donvi = Math.floor(nhapSo % 10);
    var chuc = Math.floor((nhapSo / 10) % 10);
    var tram = Math.floor(nhapSo / 100);

    var hangTram = chuyendoiChu(tram);
    var hangChuc = chuyendoiChu(chuc);
    var hangDonvi = chuyendoiChu(donvi);

    function chuyendoiChu(nhapSo){
        switch (nhapSo) {
            case 0:
            case "0":
            case 1:
            case "1":
                return "Một"    
            case 2:
            case "2":   
                return "Hai"     
            case 3:
            case "3":   
                return "Ba"    
            case 4:
            case "4":   
                return "Bốn"    
            case 5:
            case "5":   
                return "Năm"
            case 6:
            case "6":
                return "Sáu"    
            case 7:
            case "7":   
                return "Bảy"     
            case 8:
            case "8":   
                return "Tám"    
            case 9:
            case "9":   
                return "Chín"       
        default:
        }
    }
    ketqua.innerHTML = `${hangTram} trăm ${hangChuc} mươi ${hangDonvi} ngàn .`
}

// Bài 4
function tinhKhoangcach() {
    var tensv1 = document.getElementById("ten-sv-t1").value;
    var x1 = document.getElementById("toadoX-sv-t1").value * 1;
    var y1 = document.getElementById("toadoY-sv-t1").value * 1;

    var tensv2 = document.getElementById("ten-sv-t2").value;
    var x2 = document.getElementById("toadoX-sv2").value * 1;
    var y2 = document.getElementById("toadoY-sv2").value * 1;

    var tensv3 = document.getElementById("ten-sv-t3").value;
    var x3 = document.getElementById("toadoX3").value * 1;
    var y3 = document.getElementById("toadoY3").value * 1;

    var xTruong = document.getElementById("toadoX-truong").value * 1;
    var yTruong = document.getElementById("toadoY-truong").value * 1;

    var d1 = Math.sqrt(Math.pow((xTruong - x1), 2) + Math.pow((yTruong - y1), 2));
    var d2 = Math.sqrt(Math.pow((xTruong - x2), 2) + Math.pow((yTruong - y2), 2));
    var d3 = Math.sqrt(Math.pow((xTruong - x3), 2) + Math.pow((yTruong - y3), 2));
    console.log({d1,d2,d3});

    var ketqua = document.getElementById("result3");

    if (d1 > d2 && d1 > d3) {
       ketqua.innerHTML = `Sinh viên xa trường nhất: ${tensv1}`;
    } else if (d2 > d1 && d2 > d3) {
        ketqua.innerHTML = `Sinh viên xa trường nhất: ${tensv2}`;
    } else {
        ketqua.innerHTML = `Sinh viên xa trường nhất: ${tensv3}`;   
    }
}



